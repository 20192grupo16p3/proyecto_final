#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "vida.h"
#include "util.h"
#include "funciones.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>

void guardar(char **matriz,int fil, int col);
//#include "detectar.h"

//comienzo main
int main(int argc, char **argv){
	long c=0;//columnas
	long f=0;//filas
	long g=0;//generaciones
	long s=0;//microsegundos
	int  nCel=0;//numeroDeCelulas
	char op;
	long nHilos=0;
	long mostrar=0;
	while((op=getopt(argc,argv,":f:c:g:s:i:n:m"))!=-1){
		switch(op){
			case 'f':
				f=(atoi(optarg));
				break;
			case 'c':
				c=(atoi(optarg));
				break;
			case 'g':
				g=(atoi(optarg));
				break;
			case 's':
				s=(atoi(optarg));
				break;
			case 'i':
				nCel=(atoi(optarg));
				break;
			case 'n':
				nHilos=(atoi(optarg));
				break;
			case 'm':
				mostrar=1;
				break;
		}
	}
		

	//validaciones
	if(f<=0||c<=0||s<=0||nCel<=0){
		printf("\nIngreso un valor incorrecto, porfavor intentelo nuevamente\n\n");
		printf("Valores a ingresar: \n -f (filas >0) \n -c (columnas>0) \n -s (tiempo entre generaciones en milisegundos>0) \n -i (cantidad de celulas vivas inicialmente>0) \n -g (generaciones infinitas<=0; generaciones finitas>0)\n \n");
		exit(-1);//se cierra el programa
	}
	//fin validaciones
	//inicia el juego si todos los valores son correctos.
	juego_de_vida juego;
        juego.filas=f;//10
        juego.columnas=c;//20
        juego.tiempo_sleep=s;//40000
        juego.generaciones=g;//20
	juego.nHilos=nHilos;
	juego.mostrar=mostrar;
	//printf("\n %d %d\n",juego.filas,juego.columnas);
	//char**tablero=malloc(juego.filas*sizeof(char*));
        //for (int i=0; i<juego.filas; i++){
                //*(tablero+i)=malloc(juego.columnas*sizeof(char));
        	//}
	//for(int i=0; i<juego.generaciones;i++){
		char**tablero=malloc(juego.filas*sizeof(char*));
		juego.tablero=tablero;
        	for (int i=0; i<juego.filas; i++){
                	*(tablero+i)=malloc(juego.columnas*sizeof(char));
                	}
		llenar_matriz_azar(tablero,juego.filas,juego.columnas,nCel);

		if(mostrar==1){
			dibujar_grilla(tablero,juego.filas,juego.columnas);
		}

		usleep(juego.tiempo_sleep);
		vivir_morir(tablero, juego);
		guardar(juego.tablero,juego.filas,juego.columnas);


	//for(int i=0; i<juego.generaciones;i++){
		//if(i>0){vivir_morir(, juego.filas, juego.columnas);}
		for(int i=0; i<juego.filas; i++){
			free(*(tablero+i));
			}
		free(tablero);
		//dibujar_grilla(grilla_nueva,juego.filas,juego.columnas);
		//free(grilla_nueva);
		//usleep(1000000);
		//}
		//}
}


void guardar(char **matriz, int fil, int col){		
	//printf("\e[1;1H\e[2J");
	int fd = open("resultados.txt", O_RDWR | O_CREAT | O_TRUNC, 0666);
	char *linea = malloc(col + 1);	//Char nulo al final
	for(int i = 0; i < fil; i++){
		memset(linea, ' ', col+1);
		linea[col] = 0;
		for(int j = 0; j < col; j++){
			if(matriz[i][j] == 0){
				linea[j] = ' ';
				write(fd," ",1);
			}
			else if(matriz[i][j] == 1){
				linea[j] = 'x';
				write(fd,"x",1);		
			}
		}
		//printf("%s", linea);
		//printf("\n");
		fflush(stdout);
		write(fd,"\n",1);
		
	}
	free(linea);
	close(fd);
}
