#include <stdio.h>
#include <pthread.h>

#define N 8
#define M 16
#define nHilos 10

int g=0;
int generaciones=10;
const char *mensaje[2] = {"Hola Mundo", "Mundo Hola"};
const int cantidad[2] = {N, M};
sem_t principal[nHilos];	//empiezan en 1
sem_t worker[nHilos];	//empiezan en 0

sem_t mutex;

void imprime_mensaje(void *ptr) {
        int i;
        int id;

        id = *(int *) ptr;
	while(1){
		sem_wait(&worker[id]);		//Esperamos que hilo principal nos permita avanzar

		//sem_wait(&mutex);
		for(i=0; i<1; i++)
                printf("%s\n",mensaje[id]);				//variable compartida, proteger con semaforo;
		//sem_post(&mutex);

		sem_post(&principal[id]);		//Hilo notifica a main que ya ha terminado esta iteracion.
	}

        return;
}


int main(int argc, char *argv[])
{
	for(int i = 0; i < nHilos; i++){
		sem_init(&principal[i], 0, 1);
		sem_init(&worker[i],0, 0);
	}
	sem_init(&mutex,0,1);
	int primera = 1;
        pthread_t hilo[nHilos];
        int id0=0, id1=1;

	while(g<generaciones){

	for(int i = 0; i < nHilos; i++){
                	sem_wait(&principal[i]);
		}
	if(primera=1;){
        for(int i=0;i<20;i++){
        	pthread_create(&hilo[i], NULL, (void *) &imprime_mensaje, (void *) &id0);

        }}else{
			//Mostrar resultado de trabajadores
			printf("Hilos trabajadores terminaron generacion %d",(g+1));
		}
	for(int i = 0; i < nHilos; i++){
	                sem_post(&worker[i]);
		}
	}
        /**for(int i=0;i<10;i++){
        pthread_join(hilo[i], NULL);
        }**/
        return 0;
}

