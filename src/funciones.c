#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include "vida.h"
#include "funciones.h"
#include "util.h"
//#include "detectar.h"

//pthread_mutex_t mutex;

void ** vivir_morir(char **grilla, juego_de_vida juego){	
	//printf("1\n");
	int nHilos=juego.nHilos;
	sem_t mutex;
	sem_t principal[nHilos];	//empiezan en 1
	sem_t worker[nHilos];	//empiezan en 0
	for(int i = 0; i < nHilos; i++){
		sem_init(&principal[i], 0, 1);
		sem_init(&worker[i],0, 0);
		//printf("sem_init %d\n",i);
	}
	sem_init(&mutex,0,1);
	juego.principal=principal;
	juego.worker=worker;
	//pthread_mutex_init(&mutex, NULL);
	int celulas_vivas=0;
	//juego.celulas_vivas=celulas_vivas;
	int celulas_muertas=0;
	//juego.celulas_muertas=celulas_muertas;
	int cv=0;
	//juego.cv=cv;
	int cm=0;
	//juego.cm=cm;
	int cn=0;
	int cnm=0;
	int prim=1;
	int celulas_nacidas=0;
	int celulas_muriendo=0;
	int promedio_vivas;
	int promedio_muertas;
	int filas=juego.filas;
	int columnas=juego.columnas;
	int generaciones=juego.generaciones;
	int tiempo=(juego.tiempo_sleep)*1000;
	
	int muestra=juego.mostrar;
	//printf("\n %d %d\n",filas,columnas);
//primero la nueva matriz y tiene que ser llena de ceros.
	char **grilla_nueva=(char**)calloc(filas,sizeof(char*));
	for (int c=0; c<filas; c++){
        	*(grilla_nueva+c)=(char *)calloc(columnas,sizeof(char));
	}
	juego.tablero_nuevo=grilla_nueva;
	int g=1;
	int gc=1;
	//int nHilos=juego.nHilos;
	pthread_t id[nHilos];
	//for(int g=0;g<generaciones;g++){
	//int g=1;
	//int generaciones=juego.generaciones;
	while(g<=(generaciones) || generaciones <=0){
		//printf("2\n");
		/**for(int i = 0; i < nHilos; i++){
                	sem_wait(&juego.principal[i]);
			printf("sem_wait %d\n",i);
		}**/
	
	celulas_vivas=0;
	celulas_muertas=0;
	celulas_nacidas=0;
	celulas_muriendo=0;

	//actualizar((void*)&juego);
	if(prim==1){
	printf("inicia creacion.\n");
		for(int hil=0; hil<nHilos;hil++){
		sem_wait(&mutex);
		juego.k=hil;
		sem_post(&mutex);
		printf("hilo %d\n",hil);		
		//pthread_create(&id[hil+((g-1)*nHilos)],NULL,actualizar,(void*)&juego);
			pthread_create(&id[hil],NULL,actualizar,&juego);
			printf("hilo %d creado\n",hil);
			
		}
	//printf("4\n");
	prim=0;
	}
	/**for(int hil=1;hil<=nHilos;hil++){
		pthread_join(id[hil],NULL);
		//pthread_join(id[hil+((g-1)*nHilos)],NULL);
		}	**/

	for(int i = 0; i < nHilos; i++){
	                sem_post(&worker[i]);
			//printf("sem_post worker %d\n",i);
		}
	for(int i = 0; i < nHilos; i++){
	                sem_wait(&principal[i]);
			//printf("sem_wait principal %d\n",i);
		}	
	//printf("paso el sem_wait principal");
	if(g==generaciones){
		for(int i = 0; i < nHilos; i++){
	                pthread_cancel(id[i]);
			//printf("sem_cancel hilo %d\n",i);
		}
	}

	
	///free(grilla_nueva);
	//contadores
	for(int z=0;z<filas;z++){
		for(int y=0;y<columnas;y++){
			if(*(*(grilla+z)+y)==*(*(grilla_nueva+z)+y) && *(*(grilla_nueva+z)+y)==1){
				celulas_vivas=celulas_vivas+1;
				//printf("%d nueva: %d celulas vivas %d\n",*(*(grilla+z)+y),*(*(grilla_nueva+z)+y),celulas_vivas);
			}else if(*(*(grilla+z)+y)==*(*(grilla_nueva+z)+y) && *(*(grilla_nueva+z)+y)==0){
				celulas_muertas=celulas_muertas+1;
				//printf("%d nueva: %d muertas %d\n",*(*(grilla+z)+y),*(*(grilla_nueva+z)+y),celulas_muertas);
			}else if(*(*(grilla+z)+y)!=*(*(grilla_nueva+z)+y) && *(*(grilla_nueva+z)+y)==1){
				celulas_nacidas=celulas_nacidas+1;
				celulas_vivas=celulas_vivas+1;
				//printf("%d nueva: %d nacidas %d vivas %d\n",*(*(grilla+z)+y),*(*(grilla_nueva+z)+y), celulas_nacidas,celulas_vivas);
			}else if(*(*(grilla+z)+y)!=*(*(grilla_nueva+z)+y) && *(*(grilla_nueva+z)+y)==0){
				celulas_muriendo=celulas_muriendo+1;
				celulas_muertas=celulas_muertas+1;
				//printf("%d nueva: %d muriendo %d muertas %d\n",*(*(grilla+z)+y),*(*(grilla_nueva+z)+y), celulas_muriendo,celulas_muertas);
			}
			
		}	
	}//cierre contadores

	if(muestra==1){
		dibujar_grilla(grilla_nueva,filas,columnas);
	}

	for(int u=0;u<filas;u++){
		for(int v=0;v<columnas;v++){
			*(*(grilla+u)+v)=*(*(grilla_nueva+u)+v);}
		}


	cv=cv+celulas_vivas;
	cn=cn+celulas_nacidas;
	cm=cm+celulas_muertas;
	cnm=cnm+celulas_muriendo;
	promedio_vivas=cn/(gc);
	promedio_muertas=cnm/(gc);
	if(generaciones>0){g++;}
	else{printf("\n Infinitas Generaciones.....");}
	printf("\n\nGeneracion: %d\n Celulas nacidas en esta generacion: %d\n Celulas que murieron en esta generacion: %d\n",(gc),celulas_nacidas,celulas_muriendo);
	printf(" Celulas nacidas desde la generacion 0: %d\n Celulas muertas desde la generacion 0: %d\n",cn,cnm);
	printf(" Promedio de celulas nacidas por Generacion: %d\n Promedio de celulas muertas por generacion: %d\n",promedio_vivas,promedio_muertas);
	//usleep(tiempo);
	nanosleep((const struct timespec[]){{0, tiempo}}, NULL);
	//grilla=grilla_nueva;
	gc++;
	
	//for(int hil=1;hil<=nHilos;hil++){
	//	pthread_exit(&id[hil+((g-1)*nHilos)]);
	//	}

	}//aqui se ciera el while de generaciones
	
	//comienza el detector de formas
	printf("\nFormas Detectadas: \n");
	void *cantidad;
	pthread_t tid1;
	pthread_create(&tid1,NULL,bloque,&juego);
	pthread_join(tid1,&cantidad);
	printf(" Existen %ld bloques\n",(long)cantidad);

	pthread_create(&tid1,NULL,colmenaH,&juego);
	pthread_join(tid1,&cantidad);
	printf(" Existen %ld colmenas horizontales\n",(long)cantidad);

	pthread_create(&tid1,NULL,colmenaV,&juego);
	pthread_join(tid1,&cantidad);
	printf(" Existen %ld colmenas verticales\n",(long)cantidad);

	pthread_create(&tid1,NULL,hogaza,&juego);
	pthread_join(tid1,&cantidad);
	printf(" Existen %ld hogazas\n",(long)cantidad);

	pthread_create(&tid1,NULL,bote,&juego);
	pthread_join(tid1,&cantidad);
	printf(" Existen %ld botes\n",(long)cantidad);

	pthread_create(&tid1,NULL,tina,&juego);
	pthread_join(tid1,&cantidad);
	printf(" Existen %ld tinas\n",(long)cantidad);

	for(int i=0; i<filas; i++){
		free(*(grilla_nueva+i));
		}
	free(grilla_nueva);
	//printf("\n %d %d\n",filas,columnas);
	
	return 0;
}




void* actualizar(void *argv){
	//printf("01\n");
	juego_de_vida juego=*(juego_de_vida*) argv;
	//printf("\nhola\n");
	int filas=juego.filas;
	int columnas=juego.columnas;
	int nHilos=juego.nHilos;
	int hilo=(juego.k +1);
	int id=juego.k;
	//printf("\n %d %d\n",filas,columnas);
	char **grilla=juego.tablero;
	int vecinos;
	char **grilla_nueva=juego.tablero_nuevo;
	//sem_t *workers=juego.worker;
	//sem_t *principales=juego.principal;
	//for(int k=1;k<=nHilos,k++)	
	//for(int i=(n*k)-(n+(k-1)*n);i<(n*k);i++){
	int n=filas/nHilos;
	int diferencia=0;
	if(hilo==(nHilos)){
		diferencia=filas-(n*hilo);
		}
	
	//printf("\n %d %d \n", ((hilo-1)*n),(n*(hilo)+diferencia));
	while(1){
	//printf("02\n");
	sem_wait(&juego.worker[id]);
	//printf("sem_wait actualizar %d\n",id);
	//printf("03\n");
	for(int i=((hilo-1)*n);i<(n*(hilo)+diferencia);i++){ //filas

		for(int j=0;j<columnas;j++){
 
			vecinos=0;
			if(i==0 && j==0){
			//(aqui va la esquina superior izquierda)
				for(int k=0;k<2;k++){
					for(int l=0;l<2;l++){
						if(*(*(grilla+k)+l)==1){vecinos++;}
						}
					}
				if(*(*(grilla+i)+j)==1){vecinos--;} //(esto es para que reste si esta viva ya que igual la contara para poder usar los for.)
			}

			else if(i==0 && j==(columnas-1)){
			//(aqui va la esquina superior derecha)
				for(int m=0;m<2;m++){
					for(int n=columnas-2;n<columnas;n++){
						if(*(*(grilla+m)+n)==1){vecinos++;}
					}
				}
				if(*(*(grilla+i)+j)==1){vecinos--;}
			}

			else if(i==(filas-1) && j==0){
			//(aqui va la esquina inferior izquierda)
				for(int o=filas-2;o<filas;o++){
					for(int p=0;p<2;p++){
						if(*(*(grilla+o)+p)==1){vecinos++;}
					}
				}
				if(*(*(grilla+i)+j)==1){vecinos--;}
			}

			else if(i==(filas-1) && j==(columnas-1)){
			//(aqui va la esquina inferior derecha)
				for(int q=filas-2;q<filas;q++){
					for(int r=columnas-2;r<columnas;r++){
						if(*(*(grilla+q)+r)==1){vecinos++;}
					}
				}
				if(*(*(grilla+i)+j)==1){vecinos--;}
			}

			else if(i==0 && (j>0 && j<(columnas-1))){
			//(fila superior sin esquinas)
				for(int s=0;s<2;s++){
					for(int t=-1;t<2;t++){
						if(*(*(grilla+s)+j+t)==1){vecinos++;}
					}
				}
				if(*(*(grilla+i)+j)==1){vecinos--;}
			}

			else if(i==(filas-1) && (j>0 && j<(columnas-1))){
			//(fila inferior sin esquinas)
				for(int u=filas-2;u<filas;u++){
					for(int v=-1;v<2;v++){
						if(*(*(grilla+u)+j+v)==1){vecinos++;}
					}
				}
				if(*(*(grilla+i)+j)==1){vecinos--;}
			}

			else if((i>0 && i<(filas-1)) && j==0){
			//(columna lateral izquierda sin esquinas)
				for(int w=-1;w<2;w++){
					for(int x=0;x<2;x++){
						if(*(*(grilla+i+w)+x)==1){vecinos++;}
					}
				}
				if(*(*(grilla+i)+j)==1){vecinos--;}
			}

			else if((i>0 && i<(filas-1)) && j==(columnas-1)){
			//(columna lateral derecha sin esquinas)
				for(int y=-1;y<2;y++){
					for(int z=columnas-2;z<columnas;z++){
						if(*(*(grilla+i+y)+z)==1){vecinos++;}
					}
				}
				if(*(*(grilla+i)+j)==1){vecinos--;}
			}

			else{
			//(cualquiera que tenga los 8 lados es decir todas las que no estan en los bordes)
				for(int a=-1;a<2;a++){
					for(int b=-1;b<2;b++){
						if(*(*(grilla+i+a)+j+b)==1){vecinos++;}
					}
				}
				if(*(*(grilla+i)+j)==1){vecinos--;}
			}
		
		//	if(*(*(grilla+i)+j)==1){vecinos--;} //(esto es para que reste si esta viva ya que igual la contara para poder usar los for.)

			//juego.vecinos=vecinos;
//aqui ya tienes la cantidad de vecinos del punto que se esta revisando
//entonces aqui ya comienzas a validar las funciones de las reglas segun si estaba viva o muerta, te dejo mas o menos como seria
			//printf("\n fila: %d  columna: %d  vecinos: %d", i,j,vecinos);
			if(*(*(grilla+i)+j)==1){
//aqui va las reglas de las vivas segun sus vecinos y la asignas a la nueva matriz
				if(vecinos<=1){//muere por soledad
					*(*(grilla_nueva+i)+j)=0;
					//pthread_mutex_lock(&mutex);
					//juego.celulas_muertas++;
					//pthread_mutex_unlock(&mutex);
				}else{
					if(vecinos>=4){
						*(*(grilla_nueva+i)+j)=0;//muere por sobrepoblacion
						//pthread_mutex_lock(&mutex);
						//juego.celulas_muertas++;
						//pthread_mutex_unlock(&mutex);
					}else{*(*(grilla_nueva+i)+j)=1; //celula vive
						//pthread_mutex_lock(&mutex);
						//juego.celulas_vivas++;
						//pthread_mutex_unlock(&mutex);
						}
				}
			}
			else{
			//aqui van las reglas si estaba muerta segun sus vecinos y la asignas a la nueva matriz
				if(vecinos==3){
					*(*(grilla_nueva+i)+j)=1;
					//pthread_mutex_lock(&mutex);
					//juego.celulas_vivas++;
					//pthread_mutex_unlock(&mutex);
				}else{*(*(grilla_nueva+i)+j)=0;}
			}

		}//aqui se cierra el for de columnas
	}//aqui se cierra el for de filas
	//pthread_exit(NULL);
	sem_post(&juego.principal[id]);
	//printf("sem_post actualizar %d\n",id);
	}//aqui cierra el infinito



//printf("\nsoy el hilo %ld, %ld\n",juego.k, pthread_self());
return 0;
}

//funciones de control

void* bloque(void *argv){
	juego_de_vida juego=*(juego_de_vida*) argv;
	char **grilla=juego.tablero;
	int filas=juego.filas;
	int columnas=juego.columnas;
	long bloques=0;
	//juego.bloque=bloques;
	for (int i=0;i<(filas-1);i++){
		for(int j=0;j<(columnas-1);j++){
			if(*(*(grilla+i)+j)==1 && *(*(grilla+(i))+(j+1))==1 && *(*(grilla+(i+1))+(j))==1 && *(*(grilla+(i+1))+(j+1))==1){
				if(i!=0 && j!=0 && i!=(filas-2) && j!=(columnas-2) && *(*(grilla+(i-1))+(j-1))==0 && *(*(grilla+(i-1))+(j))==0 && *(*(grilla+(i-1))+(j+1))==0 && *(*(grilla+(i-1))+(j+2))==0 && *(*(grilla+(i+2))+(j-1))==0 && *(*(grilla+(i+2))+(j))==0 && *(*(grilla+(i+2))+(j+1))==0 && *(*(grilla+(i+2))+(j+2))==0 && *(*(grilla+(i))+(j-1))==0 && *(*(grilla+(i+1))+(j-1))==0 && *(*(grilla+(i))+(j+2))==0 && *(*(grilla+(i+1))+(j+2))==0){
					bloques=bloques+1;
				}
				if(i==0 || j==0 || i==(filas-2) || j==(columnas-2)){
					bloques=bloques+1;
				}
				
			}
			
		}
	}
	juego.bloque=bloques;
	return (void*)bloques;
}


void* colmenaH(void *argv){
	juego_de_vida juego=*(juego_de_vida*) argv;
	char **grilla=juego.tablero;
	int filas=juego.filas;
	int columnas=juego.columnas;
	long colmenasH=0;
	//juego.colmenaH=colmenasH;

	for (int i=0;i<(filas-2);i++){
		for(int j=0;j<(columnas-3);j++){
			if(*(*(grilla+i)+j)==0 && *(*(grilla+(i))+(j+1))==1 && *(*(grilla+(i))+(j+2))==1 && *(*(grilla+(i))+(j+3))==0 && *(*(grilla+(i+1))+j)==1 && *(*(grilla+(i+1))+(j+1))==0 && *(*(grilla+(i+1))+(j+2))==0 && *(*(grilla+(i+1))+(j+3))==1 && *(*(grilla+(i+2))+j)==0 && *(*(grilla+(i+2))+(j+1))==1 && *(*(grilla+(i+2))+(j+2))==1 && *(*(grilla+(i+2))+(j+3))==0){
				if(i!=0 && i!=(filas-3) && j!=0 && j!=(columnas-4) && *(*(grilla+(i-1))+(j-1))==0 && *(*(grilla+(i-1))+(j))==0 && *(*(grilla+(i-1))+(j+1))==0 && *(*(grilla+(i-1))+(j+2))==0 && *(*(grilla+(i-1))+(j+3))==0 && *(*(grilla+(i-1))+(j+4))==0 && *(*(grilla+(i+3))+(j-1))==0 && *(*(grilla+(i+3))+(j))==0 && *(*(grilla+(i+3))+(j+1))==0 && *(*(grilla+(i+3))+(j+2))==0 && *(*(grilla+(i+3))+(j+3))==0 && *(*(grilla+(i+3))+(j+4))==0 && *(*(grilla+(i))+(j-1))==0 && *(*(grilla+(i+1))+(j-1))==0 && *(*(grilla+(i+2))+(j-1))==0 && *(*(grilla+(i))+(j+4))==0 && *(*(grilla+(i+1))+(j+4))==0 && *(*(grilla+(i+2))+(j+4))==0){
					colmenasH=colmenasH+1;
				}
				if(i==0 || j==0 || i==(filas-3) || j==(columnas-4)){
					colmenasH=colmenasH+1;
				}
			}
		}
	}
	juego.colmenaH=colmenasH;
	return (void*)colmenasH;
}


void* colmenaV(void *argv){
	juego_de_vida juego=*(juego_de_vida*) argv;
	char **grilla=juego.tablero;
	int filas=juego.filas;
	int columnas=juego.columnas;
	long colmenaV=0;
	//juego.colmenaV=colmenaV;

	for (int i=0;i<(filas-3);i++){
		for(int j=0;j<(columnas-2);j++){
			if(*(*(grilla+i)+j)==0 && *(*(grilla+(i))+(j+1))==1 && *(*(grilla+(i))+(j+2))==0 && *(*(grilla+(i+1))+(j))==1 && *(*(grilla+(i+1))+(j+1))==0 && *(*(grilla+(i+1))+(j+2))==1 && *(*(grilla+(i+2))+(j))==1 && *(*(grilla+(i+2))+(j+1))==0 && *(*(grilla+(i+2))+(j+2))==1 && *(*(grilla+(i+3))+(j))==0 && *(*(grilla+(i+3))+(j+1))==1 && *(*(grilla+(i+3))+(j+2))==0){
				if(i!=0 && i!=(filas-4) && j!=0 && j!=(columnas-3) && *(*(grilla+(i-1))+(j-1))==0 && *(*(grilla+(i-1))+(j))==0 && *(*(grilla+(i-1))+(j+1))==0 && *(*(grilla+(i-1))+(j+2))==0 && *(*(grilla+(i-1))+(j+3))==0 && *(*(grilla+(i+4))+(j-1))==0 && *(*(grilla+(i+4))+(j))==0 && *(*(grilla+(i+4))+(j+1))==0 && *(*(grilla+(i+4))+(j+2))==0 && *(*(grilla+(i+4))+(j+3))==0 && *(*(grilla+(i))+(j-1))==0 && *(*(grilla+(i+1))+(j-1))==0 && *(*(grilla+(i+2))+(j-1))==0 && *(*(grilla+(i+3))+(j-1))==0 && *(*(grilla+(i))+(j+3))==0 && *(*(grilla+(i+1))+(j+3))==0 && *(*(grilla+(i+2))+(j+3))==0 && *(*(grilla+(i+3))+(j+3))==0){
					colmenaV=colmenaV+1;
				}
				if(i==0 || j==0 || i==(filas-4) || j==(columnas-3)){
					colmenaV=colmenaV+1;
				}
			}
		}
	}
	juego.colmenaV=colmenaV;
	return (void*)colmenaV;
}



void* hogaza(void *argv){
	juego_de_vida juego=*(juego_de_vida*) argv;
	char **grilla=juego.tablero;
	int filas=juego.filas;
	int columnas=juego.columnas;
	long hogaza=0;
	//juego.hogaza=hogaza;

	for (int i=0;i<(filas-3);i++){
		for(int j=0;j<(columnas-3);j++){
			if(*(*(grilla+i)+j)==0 && *(*(grilla+(i))+(j+1))==1 && *(*(grilla+(i))+(j+2))==1 && *(*(grilla+(i))+(j+3))==0 && *(*(grilla+(i+1))+j)==1 && *(*(grilla+(i+1))+(j+1))==0 && *(*(grilla+(i+1))+(j+2))==0 && *(*(grilla+(i+1))+(j+3))==1 && *(*(grilla+(i+2))+j)==0 && *(*(grilla+(i+2))+(j+1))==1 && *(*(grilla+(i+2))+(j+2))==0 && *(*(grilla+(i+2))+(j+3))==1 && *(*(grilla+(i+3))+j)==0 && *(*(grilla+(i+3))+(j+1))==0 && *(*(grilla+(i+3))+(j+2))==1 && *(*(grilla+(i+3))+(j+3))==0){
				if(i!=0 && i!=(filas-4) && j!=0 && j!=(columnas-4) && *(*(grilla+(i-1))+(j-1))==0 && *(*(grilla+(i-1))+(j))==0 && *(*(grilla+(i-1))+(j+1))==0 && *(*(grilla+(i-1))+(j+2))==0 && *(*(grilla+(i-1))+(j+3))==0 && *(*(grilla+(i-1))+(j+4))==0 && *(*(grilla+(i+4))+(j-1))==0 && *(*(grilla+(i+4))+(j))==0 && *(*(grilla+(i+4))+(j+1))==0 && *(*(grilla+(i+4))+(j+2))==0 && *(*(grilla+(i+4))+(j+3))==0 && *(*(grilla+(i+4))+(j+4))==0 && *(*(grilla+(i))+(j-1))==0 && *(*(grilla+(i+1))+(j-1))==0 && *(*(grilla+(i+2))+(j-1))==0 && *(*(grilla+(i+3))+(j-1))==0 && *(*(grilla+(i))+(j+4))==0 && *(*(grilla+(i+1))+(j+4))==0 && *(*(grilla+(i+2))+(j+4))==0 && *(*(grilla+(i+3))+(j+4))==0){
					hogaza=hogaza+1;
				}
				if(i==0 || j==0 || i==(filas-4) || j==(columnas-4)){
					hogaza=hogaza+1;
				}
			}
		}
	}
	juego.hogaza=hogaza;
	return (void*)hogaza;
}



void* bote(void *argv){
	juego_de_vida juego=*(juego_de_vida*) argv;
	char **grilla=juego.tablero;
	int filas=juego.filas;
	int columnas=juego.columnas;
	long bote=0;
	//juego.bote=bote;

	for (int i=0;i<(filas-2);i++){
		for(int j=0;j<(columnas-2);j++){
			if(*(*(grilla+i)+j)==1 && *(*(grilla+(i))+(j+1))==1 && *(*(grilla+(i))+(j+2))==0 && *(*(grilla+(i+1))+j)==1 && *(*(grilla+(i+1))+(j+1))==0 && *(*(grilla+(i+1))+(j+2))==1 && *(*(grilla+(i+2))+j)==0 && *(*(grilla+(i+2))+(j+1))==1 && *(*(grilla+(i+2))+(j+2))==0){
				if(i!=0 && i!=(filas-3) && j!=0 && j!=(columnas-3) && *(*(grilla+(i-1))+(j-1))==0 && *(*(grilla+(i-1))+(j))==0 && *(*(grilla+(i-1))+(j+1))==0 && *(*(grilla+(i-1))+(j+2))==0 && *(*(grilla+(i-1))+(j+3))==0 && *(*(grilla+(i+3))+(j-1))==0 && *(*(grilla+(i+3))+(j))==0 && *(*(grilla+(i+3))+(j+1))==0 && *(*(grilla+(i+3))+(j+2))==0 && *(*(grilla+(i+3))+(j+3))==0 && *(*(grilla+(i))+(j-1))==0 && *(*(grilla+(i+1))+(j-1))==0 && *(*(grilla+(i+2))+(j-1))==0&& *(*(grilla+(i))+(j+3))==0 && *(*(grilla+(i+1))+(j+3))==0 && *(*(grilla+(i+2))+(j+3))==0){
					bote=bote+1;
				}
				if(i==0 || j==0 || i==(filas-3) || j==(columnas-3)){
					bote=bote+1;
				}
			}
		}
	}
	juego.bote=bote;
	return (void*)bote;
}


void* tina(void *argv){
	juego_de_vida juego=*(juego_de_vida*) argv;
	char **grilla=juego.tablero;
	int filas=juego.filas;
	int columnas=juego.columnas;
	long tina=0;
	//juego.tina=tina;

	for (int i=0;i<(filas-2);i++){
		for(int j=0;j<(columnas-2);j++){
			if(*(*(grilla+i)+j)==0 && *(*(grilla+(i))+(j+1))==1 && *(*(grilla+(i))+(j+2))==0 && *(*(grilla+(i+1))+j)==1 && *(*(grilla+(i+1))+(j+1))==0 && *(*(grilla+(i+1))+(j+2))==1 && *(*(grilla+(i+2))+j)==0 && *(*(grilla+(i+2))+(j+1))==1 && *(*(grilla+(i+2))+(j+2))==0){
				if(i!=0 && i!=(filas-3) && j!=0 && j!=(columnas-3) && *(*(grilla+(i-1))+(j-1))==0 && *(*(grilla+(i-1))+(j))==0 && *(*(grilla+(i-1))+(j+1))==0 && *(*(grilla+(i-1))+(j+2))==0 && *(*(grilla+(i-1))+(j+3))==0 && *(*(grilla+(i+3))+(j-1))==0 && *(*(grilla+(i+3))+(j))==0 && *(*(grilla+(i+3))+(j+1))==0 && *(*(grilla+(i+3))+(j+2))==0 && *(*(grilla+(i+3))+(j+3))==0 && *(*(grilla+(i))+(j-1))==0 && *(*(grilla+(i+1))+(j-1))==0 && *(*(grilla+(i+2))+(j-1))==0&& *(*(grilla+(i))+(j+3))==0 && *(*(grilla+(i+1))+(j+3))==0 && *(*(grilla+(i+2))+(j+3))==0){
					tina=tina+1;
				}
				if(i==0 || j==0 || i==(filas-3) || j==(columnas-3)){
					tina=tina+1;
				}
			}
		}
	}
	juego.tina=tina;
	return (void*)tina;
	
}

