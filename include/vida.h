#include <semaphore.h>
#ifndef VIDA_H
#define VIDA_H

typedef struct vida{
	char **tablero;	
	char **tablero_nuevo;
	long filas;
	long columnas;
	long tiempo_sleep;
	long generaciones;
	long nHilos;
	long k;
	long mostrar;
	long bloque;
	long colmenaH;
	long colmenaV;
	long hogaza;
	long bote;
	long tina;
	sem_t *principal;
	sem_t *worker;
} juego_de_vida;



#endif 
